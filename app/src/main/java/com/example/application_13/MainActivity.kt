package com.example.application_13

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.DialogInterface
import android.icu.util.Calendar
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun open(view:View){
        val alertDialogBuilder =  AlertDialog.Builder(this)
        alertDialogBuilder.setMessage("Are you sure,You wanted to make decision")
        alertDialogBuilder.setPositiveButton("No",DialogInterface.OnClickListener { dialog, which ->
            Toast.makeText(this,"You pressed No",Toast.LENGTH_LONG).show()
        })

        alertDialogBuilder.setNegativeButton("yes",DialogInterface.OnClickListener { dialog, which ->
            Toast.makeText(this,"Done",Toast.LENGTH_LONG).show()
        })

        val alertDialog = alertDialogBuilder.create()
        alertDialog.show();
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun dateDialPicker(view:View){
        val myCalendar = Calendar.getInstance()
        val year = myCalendar.get(Calendar.YEAR)
        val month = myCalendar.get(Calendar.MONTH)
        val day = myCalendar.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(this,
            DatePickerDialog.OnDateSetListener { view, selectedYear, selectedMonth, selectedDayOfMonth ->
                val selectedDate = "$selectedDayOfMonth/${selectedMonth + 1}/${selectedYear}"
                //Toast.makeText(this,selectedDate,Toast.LENGTH_LONG).show()
                tv_date.text = selectedDate.toString()
            },year,month,day).show()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun timeDialPicker(view:View){
        val myCalendar = Calendar.getInstance()
        val hour = myCalendar.get(Calendar.HOUR_OF_DAY)
        val minute = myCalendar.get(Calendar.MINUTE)
        val is24HourView = true
        TimePickerDialog(this,TimePickerDialog.OnTimeSetListener{view, hourOfDay, minute ->
            val time_var = "$hourOfDay-$minute"
           // Toast.makeText(this,time_var,Toast.LENGTH_LONG).show()
           tv_time.text = time_var
        },hour,minute,is24HourView).show()
    }
}
